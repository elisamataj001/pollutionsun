
import HomePage from './screen/HomePage';
import CalculatingPollution from './screen/CalculatingPollution';
import TrafficPollution from './screen/TrafficPollution';
import PolutionAroundMe from './screen/PolutionAroundMe';
import PollutionData from './screen/PollutionData'
import CalculatingTraffic from './screen/CalculatingTraffic';
import TrafficData from './screen/traffic-Data'

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';


const MainNavigator = createStackNavigator({
  HomePage: HomePage,
  CalculatingPollution:CalculatingPollution,
  TrafficPollution:TrafficPollution,
  PolutionAroundMe,PolutionAroundMe,
  PollutionData:PollutionData,
  CalculatingTraffic:CalculatingTraffic,
  TrafficData:TrafficData

});

const App = createAppContainer(MainNavigator);

export default App;



