import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';


class PolutionAroundMe extends React.Component {
    static navigationOptions = {
       //sets the title to PolutionAroundMe
      title: 'PolutionAroundMe',
    };
    render() {

        //allows navigation through 
        const {navigate} = this.props.navigation;
    return (
      //  allows the styling of the page 
      <View style={styles.con}>

           {/* TouchableOpacity allows the image to be touchable 
               onPress navigates to page 'pollutionData' */}
               

          <TouchableOpacity onPress={() => navigate('PollutionData')}  >
      <View style={styles.imBox}>
        {/*  Styling of the image  */}
      <Image 
                
                 style={styles.image}
                 //source of the image, this image can be found in the 'assets' folder under the name c.gif
                  source={require('../assets/c.gif')} />
       {/* the text " 47% traffic Pollution   " is added to the page */}
      <Text style={styles.text} > 47% Pollution around me </Text>
       {/* the text " Click here   " is added to the page */}
      <Text style={styles.textt} > Click here </Text>
      </View>
      
      </TouchableOpacity>

      </View>
    );
  };
  
}
//internal styling 
  const styles = StyleSheet.create({
    con: {
        flex:1,
        backgroundColor:'black',
        alignItems:'center'
        
        
      },

      image: {
        margin:50,
        resizeMode: 'contain',
        width:290,
        height: 290,
        
        
  
        
        
        
      },
      text:{
        fontSize:30,
         color:'white',
        //color: "#FF6E40",
        textAlign:'center',
        margin:-50,
        
        
  
      },
      textt:{
        fontSize:30,
         //color:'white',
        color: "#FF6E40",
        textAlign:'center',
        margin:-155,
       
  
      },
      imBox:{
       
        borderRadius:50,
        shadowColor:'white',
        shadowOffset:{width:0, height:3},
        shadowRadius:6,
        shadowOpacity:0.36,
        elevation:8,
        }
  });
  
  export default PolutionAroundMe;
  