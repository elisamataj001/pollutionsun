import React, { useState } from 'react';
import {
  
  View, 
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';



class HomePage extends React.Component {
  static navigationOptions = {
    //the header for this page is set to Welcome 
    title: 'Welcome',
    
  };
  render() {
     //allows navigation through
    const {navigate} = this.props.navigation;
    return (
      // enables the styling of the page 
      <View  style={styles.con} >

          {/* TouchableOpacity allows the image to be touchable 
      
        onPress will navigate to CalculatingPollution*/}
      
      <TouchableOpacity onPress={() => navigate('CalculatingPollution')}  >
        <View style={styles.imBox}>
           {/*  Styling of the image  */}
      <Image 
              
                 style={styles.image}
                 //source of the image, this image can be found in the 'assets' folder under the name city.gif
                  source={require('../assets/city.gif')} />

      {/* the text " pollution around me " is added to the page */}
                  
      <Text style={styles.text} >Pollution around me</Text>
      </View>
      
      </TouchableOpacity>

       {/*TouchableOpacity allows the image to be touchable 
        once the image trafficP.png is touched it will navigate to TrafficPollution
        onPress will navigate to TrafficPollution */}  

      <TouchableOpacity onPress={() => navigate('TrafficPollution')}  >
      <View style={styles.imBox}>
         {/*  Styling of the image  */}
      <Image 
            //allows the image to be styled 
                 style={styles.image}
                 //source of the image, this image can be found in the 'assets' folder under the name car.gif
                  source={require('../assets/car.gif')} />
       {/* the text " Traffic Pollution " is added to the page */}
      <Text style={styles.text} >Traffic Pollution</Text>
      </View>
      
      </TouchableOpacity>


</View>
    );
  }
}

//internal styling 
  const styles = StyleSheet.create({
    con: {
      flex:1,
      backgroundColor:'pink',
      alignItems:'center'
      
      
    },
    image: {
      margin:50,
      resizeMode: 'contain',
           width:300,
     height: 290,
      
    
      
      
      
    },
    text:{
      fontSize:30,
       color:'white',
      
      textAlign:'center',
      margin:-50

    },
    imBox:{
    borderRadius:50,
    shadowColor:'white',
    shadowOffset:{width:0, height:3},
    shadowRadius:6,
    shadowOpacity:0.36,
    elevation:8,
    }
  
    
  });
  
   export default HomePage;
  