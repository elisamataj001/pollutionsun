import React, { useState } from 'react';
import {
 
  View, 
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';


class CalculatingPollution extends React.Component {
  
    static navigationOptions = {
      //sets the title to 'CalculatingtrafficPollution',

      title: 'CalculatingtrafficPollution',
    };
    
    render() {
       //allows navigation through 
      const {navigate} = this.props.navigation;
    return (
       //  allows the styling of the page 
      <View style={styles.con}>

             {/* TouchableOpacity allows the image to be touchable 
           onPress navigates to CalculatingTraffic 
    */}

          <TouchableOpacity onPress={() => navigate('CalculatingTraffic')}  >
      <View style={styles.imBox}>
           {/*  Styling of the image  */}
      <Image 
                //allows the image to be styled
                 style={styles.image}
                 //source of the image, this image can be found in the 'assets' folder under the name sun.gif
                  source={require('../assets/sun.gif')} />
                  {/* adds the text Calculating... to the page  */}
      <Text style={styles.text} >Calculating... </Text>
      </View>
      
      </TouchableOpacity>

      </View>
    );
  };
  
}
//internal styling 
  const styles = StyleSheet.create({
    con: {
        flex:1,
        backgroundColor:'black',
        alignItems:'center'
        
        
      },

      image: {
        margin:50,
        resizeMode: 'contain',
        width:290,
        height: 290,
        
      
      
      
        
      },
      text:{
        fontSize:30,
         color:'white',
        //color: "#FF6E40",
        textAlign:'center',
        margin:-50
  
      },
      imBox:{
        borderRadius:100,

 
     shadowColor:'white',
     shadowOffset:{width:0, height:3},
     shadowRadius:6,
     shadowOpacity:0.36,
     elevation:8,
     }
  });
  
  export default CalculatingPollution;
  