import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';


class TrafficData extends React.Component {
    static navigationOptions = {
      //sets the title to TrafficData
      title: 'TrafficData',
    };
    render() {
       //allows navigation through 
        const {navigate} = this.props.navigation;
    return (
      //  allows the styling of the page 
      <View style={styles.con}>

        {/* adds the heading TrafficData */}
      <Text style={styles.text} >TrafficData </Text>

        {/* TouchableOpacity allows the image to be touchable 
    onPress will navigate straight back to the homepage 
      */}
      <TouchableOpacity onPress={() => navigate('HomePage')}  >
       {/*  Styling of the image  */}
      <Image 
                
                
                style={styles.image}
                 //source of the image, this image can be found in the 'assets' folder under the name dummy-dataT.png
                  source={require('../assets/dummy-dataT.png')} />
                  </TouchableOpacity>

      </View>
    );
  };
  
}
//internal styling 
  const styles = StyleSheet.create({
    con: {
        flex:1,
        backgroundColor:'black',
        alignItems:'center'
        
        
      },

      image: {
        margin:50,
        resizeMode: 'contain',
        width:490,
        height: 590,
        
        
      },
      text:{
        fontSize:30,
         color:'white',
        
        textAlign:'center',
    
  
      },
      imBox:{
       
        borderRadius:50,
        shadowColor:'white',
        shadowOffset:{width:0, height:3},
        shadowRadius:6,
        shadowOpacity:0.36,
        elevation:8,
        }
  });
  
  export default TrafficData;
  