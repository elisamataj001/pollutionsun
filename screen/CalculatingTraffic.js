import React, { useState } from 'react';
import {
  
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';


class TrafficPolutionsAroundMe extends React.Component {
    static navigationOptions = {
       //the header for this page is set to TrafficPolutionsAroundMe
      title: 'TrafficPolutionsAroundMe',
    };
    render() {
       //allows navigation through 
        const {navigate} = this.props.navigation;
    return (
       // enables the styling of the page 
      <View style={styles.con}>

        {/* TouchableOpacity allows the image to be touchable 
      
         onPress will navigate to TrafficData' */}

          <TouchableOpacity onPress={() => navigate('TrafficData')}  >
            
      <View style={styles.imBox}>
         {/*  Styling of the image  */}
      <Image 
               
                 style={styles.image}
                  //source of the image, this image can be found in the 'assets' folder under the name c.gif
                  source={require('../assets/c.gif')} />
                  {/* adds text to page  */}
      <Text style={styles.text} > 59% traffic Pollution   </Text>
      <Text style={styles.textt} > Click here </Text>
      </View>
      
      </TouchableOpacity>

      </View>
    );
  };
  
}
//internal styling 
  const styles = StyleSheet.create({
    con: {
        flex:1,
        backgroundColor:'black',
        alignItems:'center'
        
        
      },

      image: {
        margin:50,
        resizeMode: 'contain',
        width:290,
        height: 290,
        
        
  
        
        
        
      },
      text:{
        fontSize:30,
         color:'white',
       
        textAlign:'center',
        margin:-50,
        
        
  
      },
      textt:{
        fontSize:30,
         //color:'white',
        color: "#FF6E40",
        textAlign:'center',
        margin:-155,
       
  
      },
      imBox:{
       
        borderRadius:50,
        shadowColor:'white',
        shadowOffset:{width:0, height:3},
        shadowRadius:6,
        shadowOpacity:0.36,
        elevation:8,
        }
  });
  
  export default TrafficPolutionsAroundMe;
  