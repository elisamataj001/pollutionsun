import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';


class PolutionData extends React.Component {
    static navigationOptions = {
       //sets the title to PollutionData
      title: 'PollutionData',
    };
    render() {
      //allows navigation through 
        const {navigate} = this.props.navigation;
    return (
      //  allows the styling of the page 
      <View style={styles.con}>
        
      <Text style={styles.text} >PollutionData </Text>

    
    {/* TouchableOpacity allows the image to be touchable 
    when the "back" button is pressed it will navigate straight back to the homepage 
   onPress will navigate to HomePage */}
    
      <TouchableOpacity onPress={() => navigate('HomePage')}  >
      {/*  Styling of the image  */}
      <Image 
                
                 style={styles.image}
                 //source of the image, this image can be found in the 'assets' folder
                  source={require('../assets/dummy-data.png')} />
                  </TouchableOpacity>

      </View>
    );
  };
  
}
//internal styling 
  const styles = StyleSheet.create({
    con: {
        flex:1,
        //background colour set to black
        backgroundColor:'black',
        // postition of page is set to center 
        alignItems:'center'
        
        
      },
      //image measurements 
      image: {
        margin:50,
        resizeMode: 'contain',
        width:490,
        height: 590,
        
        
      },
      //text size and colour 
      text:{
        fontSize:30,
         color:'white',
         
        textAlign:'center',
      
      },
      imBox:{
       
        borderRadius:50,
        shadowColor:'white',
        shadowOffset:{width:0, height:3},
        shadowRadius:6,
        shadowOpacity:0.36,
        elevation:8,
        }
  });
  
  export default PolutionData;
  